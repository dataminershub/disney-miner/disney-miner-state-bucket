terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

variable "tags_file" {
  default = "tags.json"
}

provider "aws" {
  profile = ""
  region  = "us-west-1"
}
data "aws_caller_identity" "current" {}
data "aws_region" "current" {}

locals {
  account_id = (data.aws_caller_identity.current.account_id)
  tags       = jsondecode(file(var.tags_file))

  project_prefix = "disney-miner-state-bucket-${lower(var.branch)}"

}
