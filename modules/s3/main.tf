resource "aws_s3_bucket" "example" {
  bucket = "dm.bucket.tf.state.files"

  force_destroy = true

  tags = local.tags
}